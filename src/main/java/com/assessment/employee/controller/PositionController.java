package com.assessment.employee.controller;

import com.assessment.employee.dto.RequestPositionDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.service.PositionService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/positions")
@AllArgsConstructor
public class PositionController {
    private final PositionService positionService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<List<String>>> getPositions() {
        return positionService.getPositions();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<String>> createPosition(@RequestBody @Valid RequestPositionDTO requestPositionDTO) {
        return positionService.createPosition(requestPositionDTO);
    }

    @DeleteMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<String>> deletePosition(@RequestParam String name) {
        return positionService.deleteByName(name);
    }
}
