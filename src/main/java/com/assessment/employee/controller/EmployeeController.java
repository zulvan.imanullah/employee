package com.assessment.employee.controller;

import com.assessment.employee.dto.RequestEmployeeDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.service.EmployeeService;
import lombok.AllArgsConstructor;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@AllArgsConstructor
public class EmployeeController {
    private final EmployeeService employeeService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<List<ResponseEmployeeDTO>>> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @GetMapping(value = "/{name}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> getEmployeeByName(@RequestParam String name) {
        return employeeService.getEmployeeByName(name);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<String>> createEmployee(@RequestBody @Valid RequestEmployeeDTO requestEmployeeDTO) {
        return employeeService.createEmployee(requestEmployeeDTO);
    }

    @PutMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> updateEmployee(@RequestParam String name, @RequestBody @Valid RequestEmployeeDTO requestEmployeeDTO) {
        return employeeService.updateEmployee(name, requestEmployeeDTO);
    }

    @DeleteMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<String>> deleteEmployee(@RequestParam String name) {
        return employeeService.deleteEmployee(name);
    }
}
