package com.assessment.employee.constraint;

import com.assessment.employee.model.Position;
import com.assessment.employee.repository.PositionRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PositionValidator implements ConstraintValidator<PositionConstraint, String> {
    private final PositionRepository positionRepository;

    @Override
    public boolean isValid(final String positionName, final ConstraintValidatorContext context) {
        Position position = positionRepository.findByName(positionName);
        return position != null;
    }
}
