package com.assessment.employee.service;

import com.assessment.employee.constant.Message;
import com.assessment.employee.dto.RequestPositionDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.PositionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PositionService {
    private final PositionRepository positionRepository;

    public Position findByName(String name) {
        return positionRepository.findByName(name);
    }

    public ResponseEntity<ResponseDTO<String>> createPosition(RequestPositionDTO requestPositionDTO) {
        Position position = Position.builder()
                .name(requestPositionDTO.getName())
                .build();
        positionRepository.save(position);
        ResponseDTO<String> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.POSITION_CREATED, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    };

    public ResponseEntity<ResponseDTO<List<String>>> getPositions() {
        List<Position> positions = positionRepository.findAll();
        List<String> result = positions.stream().map((Position::getName)).toList();
        ResponseDTO<List<String>> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), result, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO<String>> deleteByName(String name) {
        positionRepository.deleteByName(name);
        ResponseDTO<String> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.POSITION_DELETED, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
