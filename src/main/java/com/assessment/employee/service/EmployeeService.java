package com.assessment.employee.service;

import com.assessment.employee.constant.Message;
import com.assessment.employee.converter.EmployeeConverter;
import com.assessment.employee.dto.RequestEmployeeDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Employee;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final PositionService positionService;

    public ResponseEntity<ResponseDTO<String>> createEmployee(RequestEmployeeDTO requestEmployeeDTO) {
        Position position = positionService.findByName(requestEmployeeDTO.getName());
        Employee employee = Employee.builder()
                .name(requestEmployeeDTO.getName())
                .position(position)
                .build();
        employeeRepository.save(employee);
        ResponseDTO<String> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.EMPLOYEE_CREATED, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    public ResponseEntity<ResponseDTO<List<ResponseEmployeeDTO>>> getAllEmployees() {
        List<Employee> employees = employeeRepository.findAll();
        List<ResponseEmployeeDTO> responseEmployeeDTOList = employees.stream().map(EmployeeConverter::convertToResponseEmployeeDTO).toList();
        ResponseDTO<List<ResponseEmployeeDTO>> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTOList, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> getEmployeeByName(String name) {
        Employee employee = employeeRepository.findByName(name);
        if(employee == null) {
            ResponseDTO<ResponseEmployeeDTO> responseDTO = new ResponseDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, Message.EMPLOYEE_NOT_FOUND);
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        }
        ResponseEmployeeDTO responseEmployeeDTO = EmployeeConverter.convertToResponseEmployeeDTO(employee);
        ResponseDTO<ResponseEmployeeDTO> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> updateEmployee(String name, RequestEmployeeDTO requestEmployeeDTO) {
        Employee existingEmployee = employeeRepository.findByName(name);
        if(existingEmployee == null) {
            ResponseDTO<ResponseEmployeeDTO> responseDTO = new ResponseDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, Message.EMPLOYEE_NOT_FOUND);
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        }
        Position position = positionService.findByName(requestEmployeeDTO.getPosition());
        existingEmployee.setName(requestEmployeeDTO.getName());
        existingEmployee.setPosition(position);
        employeeRepository.save(existingEmployee);
        ResponseEmployeeDTO responseEmployeeDTO = EmployeeConverter.convertToResponseEmployeeDTO(existingEmployee);
        ResponseDTO<ResponseEmployeeDTO> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);

    }

    public ResponseEntity<ResponseDTO<String>> deleteEmployee(String name) {
        employeeRepository.deleteByName(name);
        ResponseDTO<String> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.EMPLOYEE_DELETED, null);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}

