package com.assessment.employee.constant;

public class Message {
    public static final String EMPLOYEE_NOT_FOUND = "Employee Not Found";
    public static final String EMPLOYEE_CREATED = "Employee has been successfully created";
    public static final String EMPLOYEE_DELETED = "Employee has been successfully deleted";
    public static final String POSITION_REQUIRED = "Position is required";
    public static final String POSITION_INVALID = "Position is invalid";
    public static final String NAME_REQUIRED = "Name is required";
    public static final String NAME_INVALID = "Position is invalid";
    public static final String POSITION_CREATED = "Position has been successfully created";
    public static final String POSITION_DELETED = "Position has been successfully deleted";
}
