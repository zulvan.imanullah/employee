package com.assessment.employee.exception;

public class CustomRequestInputModelValidationErrorException extends RuntimeException {
    public CustomRequestInputModelValidationErrorException(String message) {
        super(message);
    }
}
