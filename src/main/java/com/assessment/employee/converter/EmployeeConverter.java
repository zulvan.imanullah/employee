package com.assessment.employee.converter;

import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.model.Employee;

public class EmployeeConverter {
    public static ResponseEmployeeDTO convertToResponseEmployeeDTO(Employee employee) {
        return ResponseEmployeeDTO.builder()
                .name(employee.getName())
                .position(employee.getPosition().getName())
                .build();
    }
}
