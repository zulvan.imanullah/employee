package com.assessment.employee.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Status {
    ERROR("06", "FAILED"),
    SUCCESS("00", "SUCCESS");

    private final String statusCode;
    private final String description;
}
