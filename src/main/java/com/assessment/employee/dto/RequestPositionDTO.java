package com.assessment.employee.dto;

import com.assessment.employee.constant.Message;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestPositionDTO {
    @NotBlank(message = Message.NAME_REQUIRED)
    @Pattern(regexp = "^[a-zA-Z-_ ]*$", message = Message.NAME_INVALID)
    private String name;
}
