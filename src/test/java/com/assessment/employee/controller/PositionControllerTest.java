package com.assessment.employee.controller;

import com.assessment.employee.constant.Message;
import com.assessment.employee.dto.RequestPositionDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.PositionRepository;
import com.assessment.employee.service.PositionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PositionControllerTest {
    @Autowired
    private MockMvc client;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PositionService positionService;
    @Autowired
    private PositionRepository positionRepository;

    @AfterEach
    void cleanUp() {
        positionRepository.deleteAllInBatch();
    }

    @Test
    void getPositions_shouldReturnPositionsAndStatusOK_whenInvoked() throws Exception {
        String url = "/positions";
        Position position = Position.builder()
                .name("HEAD")
                .build();
        positionRepository.save(position);
        List<String> positions = singletonList(position.getName());
        ResponseDTO<List<String>> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), positions, null);
        String expectedJson = objectMapper.writeValueAsString(responseDTO);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(url);

        client.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void createPosition_shouldReturnMessageSuccessAndStatusCreated_whenInvoked() throws Exception {
        String url = "/positions";
        RequestPositionDTO requestPositionDTO = RequestPositionDTO.builder().name("HEAD").build();
        ResponseDTO<String> responseDTO = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.POSITION_CREATED, null);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(url);
        String requestBody = this.objectMapper.writeValueAsString(requestPositionDTO);
        String expectedJson = this.objectMapper.writeValueAsString(responseDTO);

        client.perform(request.contentType(MediaType.APPLICATION_JSON).content((requestBody)))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedJson));
    }
}
