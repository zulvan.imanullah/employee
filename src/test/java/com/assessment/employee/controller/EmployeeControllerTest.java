package com.assessment.employee.controller;

import com.assessment.employee.constant.Message;
import com.assessment.employee.dto.RequestEmployeeDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Employee;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.EmployeeRepository;
import com.assessment.employee.repository.PositionRepository;
import com.assessment.employee.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc client;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        Position position = Position.builder().name("HEAD").build();
        positionRepository.save(position);
    }

    @AfterEach
    void cleanUp() {
        employeeRepository.deleteAllInBatch();
        positionRepository.deleteAllInBatch();
    }

    @Test
    void getEmployee_shouldReturnListOfEmployeeAndStatusOK_whenInvoked() throws Exception {
        String url = "/employee";
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(employee.getName())
                .position(position.getName())
                .build();
        List<ResponseEmployeeDTO> employeeList = singletonList(responseEmployeeDTO);
        ResponseDTO<List<ResponseEmployeeDTO>> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), employeeList, null);
        String expectedJson = objectMapper.writeValueAsString(expectedBody);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(url);

        client.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void getEmployeeByName_shouldReturnStatusOKAndReturnCorrectlyEmployee_whenInvoked() throws Exception {
        String url = "/employee/Zulvan";
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(employee.getName())
                .position(position.getName())
                .build();
        ResponseDTO<ResponseEmployeeDTO> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);
        String expectedJson = objectMapper.writeValueAsString(expectedBody);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(url);

        client.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void createEmployee_shouldReturnStatusCreatedAndReturnSuccessMessage_whenInvoked() throws Exception {
        String url = "/employee";
        RequestEmployeeDTO requestEmployeeDTO = RequestEmployeeDTO.builder().position("HEAD").name("Zulvan").build();
        ResponseDTO<String> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.EMPLOYEE_CREATED, null);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(url);
        String requestBody = this.objectMapper.writeValueAsString(requestEmployeeDTO);
        String expectedJson = this.objectMapper.writeValueAsString(expectedBody);

        client.perform(request.contentType(MediaType.APPLICATION_JSON).content((requestBody)))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void updateEmployee_shouldReturnStatusOKAndReturnUpdatedUser_whenInvoked() throws Exception {
        String url = "/employee/Zulvan";
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        RequestEmployeeDTO requestEmployeeDTO = RequestEmployeeDTO.builder().position("HEAD").name("Zulvan2").build();
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(requestEmployeeDTO.getName())
                .position(position.getName())
                .build();
        ResponseDTO<ResponseEmployeeDTO> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(url);
        String requestBody = this.objectMapper.writeValueAsString(requestEmployeeDTO);
        String expectedJson = this.objectMapper.writeValueAsString(expectedBody);

        client.perform(request.contentType(MediaType.APPLICATION_JSON).content((requestBody)))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedJson));
    }
}
