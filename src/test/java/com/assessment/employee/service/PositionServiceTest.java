package com.assessment.employee.service;

import com.assessment.employee.constant.Message;
import com.assessment.employee.dto.RequestPositionDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.PositionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PositionServiceTest {
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private PositionService positionService;

    @AfterEach
    void cleanUp() {
        positionRepository.deleteAllInBatch();
    }

    @Test
    void getPositions_shouldReturnPositionsAndStatusOK_whenInvoked(){
        Position position = Position.builder()
                .name("HEAD")
                .build();
        positionRepository.save(position);
        List<String> positions = singletonList(position.getName());
        ResponseDTO<List<String>> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), positions, null);

        ResponseEntity<ResponseDTO<List<String>>> response = positionService.getPositions();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
    }

    @Test
    void findByName_shouldCorrectlyPosition_whenInvoked(){
        Position position = Position.builder()
                .name("HEAD")
                .build();
        positionRepository.save(position);

        Position foundedPosition = positionService.findByName(position.getName());

        assertEquals(foundedPosition.getName(), position.getName());
    }

    @Test
    void createPosition_shouldReturnMessageSuccessAndStatusCreated_whenInvoked(){
        RequestPositionDTO requestPositionDTO = RequestPositionDTO.builder().name("HEAD").build();
        ResponseDTO<String> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.POSITION_CREATED, null);

        ResponseEntity<ResponseDTO<String>> response = positionService.createPosition(requestPositionDTO);
        Position newPosition = positionService.findByName(requestPositionDTO.getName());

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
        assertEquals(newPosition.getName(), requestPositionDTO.getName());
    }
}
