package com.assessment.employee.service;

import com.assessment.employee.constant.Message;
import com.assessment.employee.dto.RequestEmployeeDTO;
import com.assessment.employee.dto.ResponseDTO;
import com.assessment.employee.dto.ResponseEmployeeDTO;
import com.assessment.employee.enumeration.Status;
import com.assessment.employee.model.Employee;
import com.assessment.employee.model.Position;
import com.assessment.employee.repository.EmployeeRepository;
import com.assessment.employee.repository.PositionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EmployeeServiceTest {
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        Position position = Position.builder().name("HEAD").build();
        positionRepository.save(position);
    }

    @AfterEach
    void cleanUp() {
        employeeRepository.deleteAllInBatch();
        positionRepository.deleteAllInBatch();
    }

    @Test
    void getEmployee_shouldReturnListOfEmployeeAndStatusOK_whenInvoked() {
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(employee.getName())
                .position(position.getName())
                .build();
        List<ResponseEmployeeDTO> employeeList = singletonList(responseEmployeeDTO);
        ResponseDTO<List<ResponseEmployeeDTO>> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), employeeList, null);

        ResponseEntity<ResponseDTO<List<ResponseEmployeeDTO>>> response = employeeService.getAllEmployees();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
    }

    @Test
    void getEmployeeByName_shouldReturnStatusOKAndReturnCorrectlyEmployee_whenInvoked(){
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(employee.getName())
                .position(position.getName())
                .build();
        ResponseDTO<ResponseEmployeeDTO> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);

        ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> response = employeeService.getEmployeeByName(employee.getName());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
    }

    @Test
    void createEmployee_shouldReturnStatusCreatedAndReturnSuccessMessage_whenInvoked(){
        RequestEmployeeDTO requestEmployeeDTO = RequestEmployeeDTO.builder().position("HEAD").name("Zulvan").build();
        ResponseDTO<String> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), Message.EMPLOYEE_CREATED, null);

        ResponseEntity<ResponseDTO<String>> response = employeeService.createEmployee(requestEmployeeDTO);
        Employee newEmployee = employeeRepository.findByName(requestEmployeeDTO.getName());

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
        assertEquals(newEmployee.getName(), requestEmployeeDTO.getName());
    }

    @Test
    void updateEmployee_shouldReturnStatusOKAndReturnUpdatedUser_whenInvoked(){
        Position position = positionRepository.findByName("HEAD");
        Employee employee = Employee.builder()
                .position(position)
                .name("Zulvan")
                .build();
        employeeRepository.save(employee);
        RequestEmployeeDTO requestEmployeeDTO = RequestEmployeeDTO.builder().position("HEAD").name("Zulvan2").build();
        ResponseEmployeeDTO responseEmployeeDTO = ResponseEmployeeDTO.builder()
                .name(requestEmployeeDTO.getName())
                .position(position.getName())
                .build();
        ResponseDTO<ResponseEmployeeDTO> expectedBody = new ResponseDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), responseEmployeeDTO, null);

        ResponseEntity<ResponseDTO<ResponseEmployeeDTO>> response = employeeService.updateEmployee(employee.getName(), requestEmployeeDTO);
        Employee updatedEmployee = employeeRepository.findByName(requestEmployeeDTO.getName());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBody, response.getBody());
        assertEquals(updatedEmployee.getName(), requestEmployeeDTO.getName());
    }
}
