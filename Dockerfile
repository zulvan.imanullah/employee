FROM mcr.microsoft.com/java/jdk:17.0.5-zulu-microsoft

COPY target/my-java-app.jar /usr/src/app/

WORKDIR /usr/src/app/

CMD ["java", "-jar", "my-java-app.jar"]
